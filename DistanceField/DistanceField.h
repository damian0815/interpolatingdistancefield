/*
 *  DistanceField.h
 *  DistanceField
 *
 *  Created by Damian Stewart on 20/11/15.
 *  Copyright (c) 2015 Damian Stewart. All rights reserved.
 *
 */

#ifndef DistanceField_H
#define DistanceField_H

#include <vector>
#include <numeric>
#include <glm/glm.hpp>
#include "Voxels.h"

using glm::vec3;
using glm::dvec3;
using std::vector;

namespace InterpolatingDistanceField
{
    template <class VectorClass>
    AABB CalculateAABB(const vector<VectorClass>& vertices);

    class DistanceField
    {
    public:
        
        struct Params
        {
            float gridSize; // size of voxels
            float maxOuterDistance; // maximum distance outwards from the AABB of the mesh to generate distance field voxels
        };
        
        // triangles is a list of vertex indices, 3 indices to define each triangle in the mesh.
        DistanceField(const vector<vec3>& vertices, const vector<size_t>& triangles, const Params& params);
        DistanceField(const vector<dvec3>& vertices, const vector<size_t>& triangles, const Params& params);
        
        struct VoxelData
        {
            float distance;
            vec3 nearestSurfacePoint;
            
            VoxelData operator*(float factor) const {
                return { .distance = distance * factor,
                    .nearestSurfacePoint = nearestSurfacePoint * factor };
            }
            VoxelData operator+(const VoxelData& other) const {
                return { .distance = distance + other.distance,
                    .nearestSurfacePoint = nearestSurfacePoint + other.nearestSurfacePoint };
            }
        };
        
        VoxelData GetRawData(vec3 pos);
        VoxelData GetInterpolatedData(vec3 pos);
        
    private:
        
        DistanceField(const Params& params, const AABB& aabb);
        

        Voxels<VoxelData> mVoxels;
        
    };


    template<class VectorClass>
    AABB CalculateAABB(const VectorClass* v, int count)
    {
        VectorClass min = std::accumulate(v, v+count, v[0], [](const VectorClass& a, const VectorClass& b) {
            return VectorClass(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z,b.z));
        });
        VectorClass max = std::accumulate(v, v+count, v[0], [](const VectorClass& a, const VectorClass& b) {
            return VectorClass(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z,b.z));
        });
        auto delta = max - min;
        min -= delta*0.0001f;
        max += delta*0.0001f;
        return { .min = vec3(min.x, min.y, min.z), .max = vec3(max.x, max.y, max.z) };
    }

    template <class VectorClass>
    AABB CalculateAABB(const vector<VectorClass>& vertices)
    {
        return CalculateAABB(&vertices[0], vertices.size());
    }
}

#endif
