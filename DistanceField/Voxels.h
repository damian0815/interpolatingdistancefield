//
//  Voxels.h
//  
//
//  Created by Damian Stewart on 20/11/15.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#pragma once

#include <vector>
#include <glm/glm.hpp>

using std::vector;
using glm::vec3;

namespace InterpolatingDistanceField {
    struct AABB {
        vec3 min;
        vec3 max;

        static bool Overlaps(const AABB& a, const AABB& b)
        {
            return(a.max.x > b.min.x && a.min.x < b.max.x &&
                   a.max.y > b.min.y && a.min.y < b.max.y &&
                   a.max.z > b.min.z && a.min.z < b.max.z);
        }
    };
    
    template <class T>
    class Voxels
    {
    public:

        Voxels(float voxelSize, AABB aabb);
        
        const T& GetVoxelValue(const vec3& p) const;
        T GetInterpolatedValue(const vec3& p) const;
        
        const struct AABB& GetAABB() const { return mAABB; }
        unsigned int GetISize() const { return mISize; }
        unsigned int GetJSize() const { return mJSize; }
        unsigned int GetKSize() const { return mKSize; }
        float GetGridSize() const { return mVoxelSize; }
        
        void SetVoxelValue(int i, int j, int k, const T& value);
        
    private:
        
        struct VoxelDefinition
        {
            unsigned int i;
            unsigned int j;
            unsigned int k;
        };
        VoxelDefinition GetVoxelDefinition(const vec3& p) const;
        vec3 GetVoxelPosition(const VoxelDefinition& def) const;
        
        unsigned int GetVoxelIndex(const VoxelDefinition& def) const;
        const T& GetVoxelValue(const VoxelDefinition& def) const;
        
        T TrilinearInterpolate(const vector<VoxelDefinition>& interpolationSources, const vec3& interpolationFactors) const;
        
        vector<T> mVoxels;
        
        float mVoxelSize;
        unsigned int mISize, mJSize, mKSize;
        AABB mAABB;
    };
    
    template<class T>
    Voxels<T>::Voxels(float voxelSize, AABB aabb)
    : mVoxelSize(voxelSize), mAABB(aabb)
    {
        // round aabb.max up to multiple of voxelSize
        vec3 range = mAABB.max - mAABB.min;
        mISize = (int)std::ceilf(range.x/mVoxelSize);
        mJSize = (int)std::ceilf(range.y/mVoxelSize);
        mKSize = (int)std::ceilf(range.z/mVoxelSize);
        mVoxels.resize(mISize*mJSize*mKSize);
        range.x = mISize * mVoxelSize;
        range.y = mJSize * mVoxelSize;
        range.z = mKSize * mVoxelSize;
        mAABB.max = mAABB.min + range;
        
    }
    
    template<class T>
    T Voxels<T>::TrilinearInterpolate(const vector<VoxelDefinition>& interpolationSources, const vec3& interpolationFactors) const
    {
        auto c00 = GetVoxelValue(interpolationSources[0]) * (1 - interpolationFactors.x) + GetVoxelValue(interpolationSources[1]) * interpolationFactors.x;
        auto c10 = GetVoxelValue(interpolationSources[2]) * (1 - interpolationFactors.x) + GetVoxelValue(interpolationSources[3]) * interpolationFactors.x;
        auto c01 = GetVoxelValue(interpolationSources[4]) * (1 - interpolationFactors.x) + GetVoxelValue(interpolationSources[5]) * interpolationFactors.x;
        auto c11 = GetVoxelValue(interpolationSources[6]) * (1 - interpolationFactors.x) + GetVoxelValue(interpolationSources[7]) * interpolationFactors.x;
        
        auto c0 = c00 * (1 - interpolationFactors.y) + c10 * interpolationFactors.y;
        auto c1 = c01 * (1 - interpolationFactors.y) + c11 * interpolationFactors.y;
        
        return c0 * (1 - interpolationFactors.z) + c1 * interpolationFactors.z;
    }
    
    template <class T>
    const T& Voxels<T>::GetVoxelValue(const vec3& p) const
    {
        auto def = GetVoxelDefinition(p);
        return GetVoxelValue(def);
    }
    
    
    template <class T>
    T Voxels<T>::GetInterpolatedValue(const vec3& p) const
    {
        auto def = GetVoxelDefinition(p);
        def.i = std::max((unsigned)1, (unsigned int)std::min(def.i, mISize-2));
        def.j = std::max((unsigned)1, (unsigned int)std::min(def.j, mJSize-2));
        def.k = std::max((unsigned)1, (unsigned int)std::min(def.k, mKSize-2));
        
        auto voxelPosition = GetVoxelPosition(def);
        const vec3 voxelCenterOffset(mVoxelSize/2, mVoxelSize/2, mVoxelSize/2);
        auto voxelRelativePos = p - voxelPosition;
        
        vector<VoxelDefinition> interpolationSources(8);
        int iInc0 = (voxelRelativePos.x < 0) ? -1 : 0;
        int iInc1 = (voxelRelativePos.x < 0) ?  0 : 1;
        int jInc0 = (voxelRelativePos.y < 0) ? -1 : 0;
        int jInc1 = (voxelRelativePos.y < 0) ?  0 : 1;
        int kInc0 = (voxelRelativePos.z < 0) ? -1 : 0;
        int kInc1 = (voxelRelativePos.z < 0) ?  0 : 1;
        int offsets[8][3] = {
            {iInc0, jInc0, kInc0},
            {iInc1, jInc0, kInc0},
            {iInc0, jInc1, kInc0},
            {iInc1, jInc1, kInc0},
            {iInc0, jInc0, kInc1},
            {iInc1, jInc0, kInc1},
            {iInc0, jInc1, kInc1},
            {iInc1, jInc1, kInc1},
        };
        for (int idx=0; idx<8; idx++) {
            interpolationSources[idx].i = def.i + offsets[idx][0];
            interpolationSources[idx].j = def.j + offsets[idx][1];
            interpolationSources[idx].k = def.k + offsets[idx][2];
        }
        
        auto interpolationFactors = voxelRelativePos / mVoxelSize;
        return TrilinearInterpolate(interpolationSources, interpolationFactors);
    }
    
    template <class T>
    unsigned int Voxels<T>::GetVoxelIndex(const Voxels::VoxelDefinition& def) const
    {
    #ifdef DEBUG
        assert(def.i < mISize && "i out of bounds");
        assert(def.j < mJSize && "j out of bounds");
        assert(def.k < mKSize && "k out of bounds");
#endif
        unsigned int index = def.i + mISize*(def.j + mJSize*def.k);
        return index;
    }
    
    template<class T>
    const T& Voxels<T>::GetVoxelValue(const Voxels::VoxelDefinition& def) const
    {
        auto index = GetVoxelIndex(def);
        return mVoxels[index];
    }
    
    template<class T>
    struct Voxels<T>::VoxelDefinition Voxels<T>::GetVoxelDefinition(const vec3& p) const
    {
        auto pLocal = p - mAABB.min;
        int i = pLocal.x / mVoxelSize;
        int j = pLocal.y / mVoxelSize;
        int k = pLocal.z / mVoxelSize;
        i = std::min(std::max(i, 0), (int)mISize-1);
        j = std::min(std::max(j, 0), (int)mJSize-1);
        k = std::min(std::max(k, 0), (int)mKSize-1);
        return (VoxelDefinition) { .i = (unsigned int)i, .j = (unsigned int)j, .k = (unsigned int)k };
    }
    
    template<class T>
    vec3 Voxels<T>::GetVoxelPosition(const struct Voxels<T>::VoxelDefinition &def) const
    {
        return mAABB.min + vec3(def.i*mVoxelSize, def.j*mVoxelSize, def.k*mVoxelSize);
    }
    
    template<class T>
    void Voxels<T>::SetVoxelValue(int i, int j, int k, const T& value)
    {
        VoxelDefinition def { .i=(unsigned int)i, .j=(unsigned int)j, .k=(unsigned int)k };
        auto index = GetVoxelIndex(def);
        mVoxels[index] = value;
    }
}
