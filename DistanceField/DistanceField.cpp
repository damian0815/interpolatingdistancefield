
#include "DistanceField.h"
#include <numeric>

#include "makelevelset3.h"

namespace InterpolatingDistanceField
{


    
    static AABB ExpandAABB(const struct AABB& aabb, float expandAmount)
    {
        return { .min = vec3(aabb.min.x-expandAmount, aabb.min.y-expandAmount, aabb.min.z-expandAmount), .max = vec3(aabb.max.x+expandAmount, aabb.max.y+expandAmount, aabb.max.z+expandAmount) };
    }
    
    
    template <class VectorClass>
    static void BuildDistanceField(Voxels<DistanceField::VoxelData>& voxels,const vector<VectorClass>& vertices, const vector<size_t>& triangles)
    {
        std::vector<Vec3ui> trianglesConverted;
        for (int i=0; i<triangles.size(); i+=3)
        {
            Vec3ui triangle((unsigned int)triangles[i], (unsigned int)triangles[i+1], (unsigned int)triangles[i+2]);
            trianglesConverted.push_back(triangle);
        }
        std::vector<Vec3f> verticesConverted;
        for (const auto& v: vertices) {
            verticesConverted.push_back(Vec3f(v.x, v.y, v.z));
        }
        
        vec3 origin = voxels.GetAABB().min;
        Vec3f originConverted(origin.x, origin.y, origin.z);
        Array3f distances;
        Array3Vec3f nearestPoints;
        int exactBands = 5;
        
        make_level_set3(trianglesConverted, verticesConverted, originConverted, voxels.GetGridSize(), voxels.GetISize(), voxels.GetJSize(), voxels.GetKSize(), distances, nearestPoints, exactBands);
        
        for (int i=0; i<distances.ni; i++) {
            //printf("i=%i:\n", i);
            for (int j=0; j<distances.nj; j++) {
                for (int k=0; k<distances.nk; k++) {
                    Vec3f nearestPoint = nearestPoints(i,j,k);
                    DistanceField::VoxelData data {
                        .distance = distances(i,j,k),
                        .nearestSurfacePoint = vec3(nearestPoint[0], nearestPoint[1], nearestPoint[2])
                    };
                    //printf(" %4.1f", data.distance);
                    voxels.SetVoxelValue(i, j, k, data);
                }
                //printf("\n");
            }
        }
    }
    
    DistanceField::DistanceField(const Params& params, const AABB& aabb)
    : mVoxels(params.gridSize, ExpandAABB(aabb, params.maxOuterDistance))
    {
    }
    
    DistanceField::DistanceField(const vector<vec3>& vertices, const vector<size_t>& triangles, const Params& params)
    : DistanceField(params, CalculateAABB(vertices))
    {
        BuildDistanceField(mVoxels, vertices, triangles);
    }
    
    DistanceField::VoxelData DistanceField::GetRawData(vec3 pos)
    {
        return mVoxels.GetVoxelValue(pos);
    }
    
    DistanceField::VoxelData DistanceField::GetInterpolatedData(vec3 pos)
    {
        return mVoxels.GetInterpolatedValue(pos);
    }
    
    
    
}