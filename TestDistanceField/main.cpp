//
//  main.cpp
//  TestDistanceField
//
//  Created by Damian Stewart on 20/11/15.
//  Copyright (c) 2015 Damian Stewart. All rights reserved.
//

#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "DistanceField.h"

using std::string;

using InterpolatingDistanceField::DistanceField;

static void FlipTriangleNormals(vector<int>& triangles)
{
    for (int i=0; i<triangles.size(); i+=3) {
        auto tmp = triangles[i];
        triangles[i] = triangles[i+2];
        triangles[i+2] = tmp;
    }
}

float round(float f, int decimalPlaces)
{
    float decimalFactor = powf(10, decimalPlaces);
    int mulled = int((f * decimalFactor)+(f<0?-0.5f:0.5f));
    return mulled / decimalFactor;
}

#define fapproxequal(a,b,tolerance) (fabsf(a-b) < tolerance)

string toString(vec3 v, const char* floatFormat="%6.2f")
{
    char format[1024];
    snprintf(format, 1024, "%s %s %s", floatFormat, floatFormat, floatFormat);
    char buf[1024];
    snprintf(buf, 1024, format, v.x, v.y, v.z);
    return buf;
}

TEST_CASE( "Distance field can be created and queried" , "[DistanceField]" ) {
    

    DistanceField::Params params { .gridSize = 0.05f, .maxOuterDistance=0.1f };
    
    
    vector<vec3> vertices {
        vec3(-0.5, -0.5, -0.5), // 0
        vec3( 0.5, -0.5, -0.5), // 1
        vec3(-0.5,  0.5, -0.5), // 2
        vec3( 0.5,  0.5, -0.5), // 3
        vec3(-0.5, -0.5,  0.5), // 4
        vec3( 0.5, -0.5,  0.5), // 5
        vec3(-0.5,  0.5,  0.5), // 6
        vec3( 0.5,  0.5,  0.5), // 7
        vec3( -0.2,  0.2,  0.2), // 8
    };
    vector<size_t> triangles {
        0,1,2, 1,3,2,
        0,4,5, 0,5,1,
        1,5,7, 1,7,3,
        2,3,6, 3,7,6,
        6,7,4, 4,7,5,
        2,6,0, 6,4,0,
    };
    
    //FlipTriangleNormals(triangles);
    
    DistanceField distanceField(vertices, triangles, params);
    
    

    REQUIRE( round(distanceField.GetInterpolatedData(vec3(0.5,0,0)).distance, 5) == 0.0f );
    REQUIRE( round(distanceField.GetInterpolatedData(vec3(0, 0.42, 0)).distance, 5) == -0.08f );
    REQUIRE( round(distanceField.GetInterpolatedData(vec3(-0.49, 0.43, 0.3)).distance, 5) == -0.01f );
    REQUIRE( round(distanceField.GetInterpolatedData(vec3(0.375, 0.0, 0.0)).distance, 5) == -0.125 );
    
    for (int i=0; i<30; i++) {
        vec3 p(0, 0, -1.0f+i*0.1f);
        auto data = distanceField.GetInterpolatedData(p);
        printf("%s: %8.5f (%s)\n", toString(p, "%3.1f").c_str(), data.distance, toString(data.nearestSurfacePoint).c_str());
    }
    
    
};

